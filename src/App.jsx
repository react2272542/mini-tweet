import {SayHello} from "./SayHello";
import {Tweet} from "./Tweet";

const App = () => {
    return (
        <div className={"tweet-container"}>
            <Tweet/>
            <Tweet/>
            <Tweet/>
            <Tweet/>
        </div>
    )
}
export default App